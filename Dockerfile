FROM adoptopenjdk/openjdk11:alpine
RUN addgroup -S app && adduser -S app -G app
USER app:app
COPY target/*.jar /app/app.jar
ENTRYPOINT ["sh", "-c", "java ${JAVA_OPTS} -jar /app/app.jar"]
