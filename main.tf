terraform {
  required_version = ">= 0.12"
  required_providers {
    aws = {
      version = "~> 3.5.0"
    }
  }
}

provider "aws" {
  region = "eu-central-1"
  profile = "dev" # Presumes that you have configured aws with `aws configure --profile dev`
}

# Create container registry to store docker image
resource "aws_ecr_repository" "container_repository" {
  name = "sample-aws-s3-storage-service"
  image_scanning_configuration {
    scan_on_push = true
  }
}

output "container_repository_url" {
  value = aws_ecr_repository.container_repository.repository_url
  description = "URL of container repository."
}

