# Building

`mvn spring-boot:build-image -Dspring-boot.build-image.imageName=tschmitz-dev/aws-s3-storage-service`

# Run Application

`docker run --rm -it -p 8000:8080 -e AWS_ACCESS_KEY=<key> -e AWS_SECRET_KEY=<key> tschmitz-dev/aws-s3-storage-service:latest`

# AWS CLI Snippets

`aws ec2 describe-images --filters "Name=name,Values=ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"`

`aws ec2 describe-tags --filters "Name=resource-type,Values=key-pair" --region "eu-central-1"`

`aws sts decode-authorization-message --encoded-message <replace-with-encoded-message> --query DecodedMessage --output text | jq '.'`