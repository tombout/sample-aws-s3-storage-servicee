package com.example.filestorage;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.PutObjectResponse;

import java.io.File;
import java.util.Objects;

/**
 * AWS S3 Client
 */
@Service
public class AwsS3StorageClient implements StorageClient {

    private final String bucketName;

    private final S3Client s3Client;

    public AwsS3StorageClient(S3Client s3Client, @Value("${aws.s3.bucket-name}") String bucketName) {
        this.s3Client = s3Client;
        this.bucketName = bucketName;
    }

    @Override
    public int save(File file, String key) {
        Objects.requireNonNull(file);

        PutObjectRequest request = PutObjectRequest.builder()
                .bucket(bucketName)
                .key(key)
                .build();

        PutObjectResponse response = s3Client.putObject(request, RequestBody.fromFile(file));

        return response.sdkHttpResponse().statusCode();
    }

}
