package com.example.filestorage;

import java.io.File;

public interface StorageClient {

    int save(File file, String key);
}
