package com.example.filestorage;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Objects;
import java.util.UUID;

@RestController
@RequestMapping("/storage")
public class StorageController {

    private final StorageClient storageClient;

    public StorageController(StorageClient storageClient) {
        this.storageClient = storageClient;
    }

    @GetMapping
    public String get() {
        return "ok";
    }

    @PostMapping
    public String uploadFile(@RequestPart(value = "file") MultipartFile file) throws IOException {
        File toFile = convertMultiPartToFile(file);

        String key = UUID.randomUUID().toString();
        storageClient.save(toFile, key);

        toFile.delete();

        return key;
    }

    private File convertMultiPartToFile(MultipartFile multipartFile) throws IOException {
        Objects.requireNonNull(multipartFile);
        Objects.requireNonNull(multipartFile.getOriginalFilename());

        File convFile = new File(multipartFile.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(multipartFile.getBytes());
        fos.close();
        return convFile;
    }
}
