package com.example.filestorage;

import cloud.localstack.Localstack;
import cloud.localstack.docker.LocalstackDockerExtension;
import cloud.localstack.docker.annotation.LocalstackDockerProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.CreateBucketRequest;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(LocalstackDockerExtension.class)
@LocalstackDockerProperties(services = {"s3"})
class AwsS3StorageClientTest {

    public static final String BUCKET_NAME = "unit-test-bucket";

    private S3Client s3Client;

    @BeforeEach
    void setUp() throws URISyntaxException {
        URI endpoint = new URI(Localstack.INSTANCE.getEndpointS3());
        AwsBasicCredentials credentials = AwsBasicCredentials.create("key", "secretKey");
        StaticCredentialsProvider credentialsProvider = StaticCredentialsProvider.create(credentials);
        s3Client = S3Client.builder()
                .credentialsProvider(credentialsProvider)
                .endpointOverride(endpoint)
                .region(Region.EU_CENTRAL_1)
                .build();
        CreateBucketRequest createBucketRequest = CreateBucketRequest.builder().bucket(BUCKET_NAME).build();
        s3Client.createBucket(createBucketRequest);
    }

    @Test
    public void shouldUploadImageToAwsS3AndReturnStatus200() throws IOException {
        StorageClient uploadService = new AwsS3StorageClient(s3Client, BUCKET_NAME);
        File fileToUpload = File.createTempFile("test", "jpg");

        int status = uploadService.save(fileToUpload, "someKey");
        assertThat(status).isEqualTo(200);
    }
}